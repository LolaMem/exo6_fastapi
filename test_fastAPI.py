# # Example from: https://fastapi.tiangolo.com/

# from typing import Union

# from fastapi import FastAPI

# # app = FastAPI()


# # @app.get("/")
# # def read_root():
# #     return {"Hello": "World"}

# # @app.get("/items/{item_id}")
# # def read_item(item_id: int, q: Union[str, None] = None):
# #     return {"item_id": item_id, "q": q}

# ##------------------------------------------------------##

# app = FastAPI()

# # To run the server:
# # uvicorn main:app --reload

# @app.get("/")
# def read_root():
#     return {"Hello": "World"}

# courses = ["mandarinas", "melones", "mangos", "sardinas", "abanicos"]

# @app.get("/courses")
# def get_course() -> List[str]:
#     return courses

# # @app.get("/vendredi/{quoi}")
# # def new(quoi:str):
# #     return {"heure":"17", "info":quoi}

# @app.get("/items/{item_id}")
# def read_item(item_id: int, q: Union[str, None] = None):
#     return {"item_id": item_id, "q": q}

# # Output:  Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
