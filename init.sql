
DROP DATABASE IF EXISTS Orders_DB;

CREATE DATABASE Orders_DB;
use Orders_DB;

CREATE TABLE Address (
  id_address INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  street VARCHAR(50),
  zipcode VARCHAR(8),
  city VARCHAR(20),
  state VARCHAR(20),
  country VARCHAR(20)
);

CREATE TABLE Client (
  id_client INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  last_name VARCHAR(20),
  first_name VARCHAR(10), 
  email VARCHAR(20),
  phone VARCHAR(20),
  default_address INT, 
  FOREIGN KEY (default_address) REFERENCES Address(id_address)
);

CREATE TABLE Client_address (
  id_address INT,
  id_client INT,
  FOREIGN KEY (id_address) REFERENCES Address(id_address),
  FOREIGN KEY (id_client) REFERENCES Client(id_client)
);

CREATE TABLE Product (
  id_product INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(20),
  description VARCHAR(250),
  image VARCHAR(250),
  price FLOAT,
  available TINYINT(1)
);

CREATE TABLE Orders (
  id_order INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_client INT,
  delivery_address INT,
  billing_address INT,
  status ENUM('cart','validated', 'sent', 'delivered'),
  payment_method ENUM('card', 'check', 'cash'),
  order_date DATE,
  FOREIGN KEY (delivery_address) REFERENCES Address(id_address),
  FOREIGN KEY (billing_address) REFERENCES Address(id_address)
);

CREATE TABLE Order_line (
  id_order INT,
  id_product INT,
  quantity INT,
  FOREIGN KEY (id_order) REFERENCES Orders(id_order),
  FOREIGN KEY (id_product) REFERENCES Product(id_product)
);

/*
CREATE TABLE Order_status_history (
    id_history INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    id_order INT,
    old_status ENUM('cart','validated', 'sent', 'delivered'),
    new_status ENUM('cart','validated', 'sent', 'delivered'),
    change_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (id_order) REFERENCES Orders(id_order)
);
*/

