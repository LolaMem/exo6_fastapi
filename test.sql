/* USE Orders_DB;*/
-- USE Orders_DB;

SOURCE init.sql

SELECT 'Create 2 new products Truc & Bidule';
INSERT  INTO Product (name, description, image, price, available)
VALUES ('Truc', 'Lorem Ipsum', 'https://picsum.photos/200', 200, 1),
       ('Bidule', 'Lorem merol', 'https://picsum.photos/100', 500, 1);

SELECT  * FROM Product;

SELECT name, price FROM Product;

SELECT 'Retrieve description and image from Truc';
SELECT description, image FROM Product WHERE name LIKE 'Truc';

-- Create Client Bob. Keep his id in a variable id_bob
INSERT  INTO Client (first_name, email) VALUES ('Bob', 'bob@gmail.com');
SET @id_bob = LAST_INSERT_ID();

SELECT * FROM Client;

SELECT 'Create new order. Keep its id in a variable id_my_order';
INSERT INTO Orders VALUES ();
SET @id_my_order = LAST_INSERT_ID();

SELECT * FROM Orders;

-- Create variables to keep  products last added
SELECT @id_truc := id_product from Product where name like 'Truc';
SELECT @id_bidule := id_product from Product where name like 'Bidule';

SELECT 'Create an Order_Line associating the last order and the products Truc & Bidule';
INSERT  INTO Order_line (id_order, id_product, quantity) 
VALUES (@id_my_order, @id_truc, 3),
       (@id_my_order, @id_bidule, 1);

SELECT * FROM Order_line;

-- Update Client Bob, by adding his phone
UPDATE Client SET phone = '0123456789' WHERE first_name = 'Bob';
SELECT * FROM Client;

SELECT 'Change the status of the order (from NULL to cart)';
UPDATE Orders SET status = 'cart' WHERE id_order = @id_my_order;

SELECT 'Update the Order associating Bob to the last order';
UPDATE Orders SET id_client = @id_bob WHERE id_order = @id_my_order;
SELECT * FROM Orders;

SELECT 'Insert an address, and associate it to billing adress in the order';
INSERT  INTO Address (street, zipcode, city, state, country)  
VALUES ('12 street St', '12345', 'Schenectady', 'New York', 'US');
SET @id_address_US = LAST_INSERT_ID();
SELECT * FROM Address;

UPDATE Orders SET billing_address = @id_address_US WHERE id_order = @id_my_order;
SELECT * FROM Orders;

SELECT 'Create a new address, and associate it to Bob, and to delivery address in the order';
INSERT  INTO Address (street, zipcode, city, state, country) 
VALUES ('12 boulevard de Strasbourg', '31000', 'Toulouse', 'OC', 'France');
SET @id_address_Bob = LAST_INSERT_ID();
SELECT * FROM Address;

UPDATE Client SET default_address = @id_address_Bob WHERE first_name LIKE 'Bob';
SELECT * FROM Client;

UPDATE Orders SET delivery_address = @id_address_Bob WHERE id_order = @id_my_order;
SELECT * FROM Orders;

-- Change again the status of the order (from cart to validated) and add payment method
UPDATE Orders SET status = 'validated' WHERE id_order = @id_my_order;

UPDATE Orders SET payment_method = 'card' WHERE id_order = @id_my_order;
SELECT * FROM Orders;

UPDATE Orders SET status = 'sent' WHERE id_order = @id_my_order;
SELECT * FROM Orders;

-- Get the list of addresses associated to Bob !!!!!!!!!!!!!!!!!!!!!!!
-- SELECT * FROM Client_address;
INSERT INTO Client_address (id_address, id_client)
SELECT default_address, id_client FROM Client WHERE id_client = @id_Bob;
SELECT * FROM Client_address;

INSERT INTO Client_address (id_address, id_client)
SELECT delivery_address, id_client FROM Orders;

INSERT INTO Client_address (id_address, id_client)
SELECT billing_address, id_client FROM Orders;

SELECT * FROM Client_address;

-- SELECT * FROM Address;
-- SELECT id_address, city FROM Address WHERE id_address = @id_adress_Bob;
-- SELECT id_address, city FROM Address WHERE id_address = @id_adress_US;
-- SELECT * FROM Orders;


-- Get the list of orders made by Bob
-- SELECT Orders.id_order, Client.id_client, Client.first_name FROM Orders
-- JOIN Client ON Orders.id_client = Client.id_client
-- WHERE Client.first_name = 'Bob'



