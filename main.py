# Main file to connect with the server

from fastapi import FastAPI
import mysql.connector
from model_dto import Product

mydb = mysql.connector.connect(
    host="localhost",
    user="lolam",
    password="lolam1",
    database="Orders_DB"
)

# uvicorn main:app --reload

app = FastAPI(debug=True)


@app.get('/')
def home():
    return "well done Lola"


# @app.get("/items/{item_id}")
# def read_item(item_id: int, q: Union[str, None] = None):
#     return {"item_id": item_id, "q": q}


@app.get("/client")
async def get_clients() -> list:
    mycursor = mydb.cursor(dictionary=True)
    sql = "select * from Client;"
    mycursor.execute(sql)
    list_of_client = mycursor.fetchall()
    return list_of_client

@app.get("/product")
async def get_products() -> list:
    mycursor = mydb.cursor(dictionary=True)
    sql = "select * from Product;"
    mycursor.execute(sql)
    list_of_product = mycursor.fetchall()
    return list_of_product

@app.get('/product/{id_product')           # Angelo
def get_product_by_id(id_product: int):
    query = "SELECT * FROM product WHERE id_product = %s"
    cursor.execute(query, (id_product,))
    result = cursor.fetchone()
    if result:
        product = {
            'id_product': result[0],
            'name': result[1],
            'description': result[2],
            'image': result[3],
            'price': result[4],
            'available': result[5]
        }
        return product
    else:
        return {"message": "Product not found"}

@app.post("/product")
async def create_product(product: Product):
    mycursor = mydb.cursor()
    sql = "insert into product(name) values (%s);"
    val = (product.name,)
    mycursor.execute(sql, val)
    mydb.commit()
    product.id = mycursor.lastrowid
    return product