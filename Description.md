Tables in the BBDD: Client, Adress, Client_Adress, Order, Product, Payment_Method, Order_Status, Order_Line

-Client: contains the personal info of the client, this info must be deleted at the demand of the client, except the defaut_adress. The Primary Key (PK) is the id_client. It relates to the table Order, One to Many. It relates to the table Adress Many to Many (delivery adress can change), through table Client_adress.

-Adress: contains the different adresses for each client (billing adress, potential miltiple delivery adress). It has a PK, id_adress. Relates to Client and also to table Order 2 to Many

-Client_adress: register all the different adresses for each client. Contains 2 Foreign Key (FK): client_id and adress_id

-Product: id_product PK, and details of the product, avaliability (True/False). Relates to the table Order Many to Many, through the table Order_line

-Order_line: link between orders and products, contains FK: id_order and id_product, and the quantity for each product.

-Order_status_history: to register the changes in status for each order. It has an id_history as PK, old_status and new_status with ENUM (cart, validated, sent and delivered), and change_date. Also id_order as FK. It relates to table Order One to Many 

-Order: id_order PK, como FK: client_id, billing_adress, delivery_adress, id_status. Also contains: payment_method (as ENUM, with values: credit card, bank check or cash at delivery), order_date