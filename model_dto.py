#File DTO: Data Transfer Object, stocks the objects/classes created

from pydantic import BaseModel
from typing import Union


class Product(BaseModel):
    #id_product: int | None = None   
    id_product: Union[int, None] = None
    name: str
    description: str
    image: str
    price: float
    available: bool

class Client(BaseModel):
    id_client: Union[int, None] = None
    last_name: str
    first_name: str
    email: str
    phone: str
    defaut_address: Union[int, None] = None

class Orders(BaseModel):
    id_order: Union[int, None] = None
    id_client: Union[int, None] = None
    delivery_address: Union[int, None] = None
    billing_address: Union[int, None] = None
    status: str
    payment_method: str
    order_date: str              #datetime???

class Order_Line(BaseModel):
    id_order: Union[int, None] = None
    id_product: Union[int, None] = None
    quantity: int               # | None = None???

class Address(BaseModel):
    id_address: Union[int, None] = None
    street: str
    zipcode: str
    city: str
    state: str
    country: str

class Client_address(BaseModel):
    id_address: Union[int, None] = None
    id_client: Union[int, None] = None

